/* 用Toast方式显示 */
Toast toast = Toast.makeText(EX05_07.this, mTextView.getText(),
        Toast.LENGTH_LONG);
View textView = toast.getView();
lay.setOrientation(LinearLayout.HORIZONTAL);
/* 在Toast里加上图片 */
mView01.setImageResource(R.drawable.icon);
/* 在Toast里显示图片 */lay.addView(mView01);
/* 在Toast里显示文字 */lay.addView(textView);
toast.setView(lay);
toast.show();
